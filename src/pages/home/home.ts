import { SamplePage } from './../sample/sample';
import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { FCM } from '@ionic-native/fcm';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private platform: Platform,
    private fcm: FCM,
    private navCtrl : NavController) {
    this.onNotification();
  }

  async onNotification() {
    await this.platform.ready();
    console.log("Chamamamamamm");


    this.fcm.getToken().then(token => {
      
    })

    this.fcm.subscribeToTopic('marketing');

    this.fcm.onNotification().subscribe(data=>{
      if(data.wasTapped){
        if(data.Page == "sample page"){
          this.navCtrl.push(SamplePage)
        }
        console.log(data.Page,'data.Page');
      } else {
        console.log(data.Page,'data.Page');
        if(data.Page == "sample page"){
          this.navCtrl.push(SamplePage)
        }
      };
    })

  }


}
